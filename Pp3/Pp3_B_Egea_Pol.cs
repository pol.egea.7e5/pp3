﻿/*
 *  Author:Pol Egea
 *  Date:21/12/21
 *  Description: Menuamb el qual es troben els exercicis del exàmen, els cuals es basen en: Crear un vector a partir d'una mida donada i que s'ompli de números, omplir dues matrius de 4x4 i intercambiar els seus valors, crear un vector de 10 lletres ordenarles i printar per pantalla, fer un programa que llegeixi les frases per teclat i et mostri la que troba més 'a'.
 */

using System;

namespace Pp3
{
    internal class Program
    {
        public static void Main()
        {
            int numero;
            do
            {
                Console.WriteLine("Benvolgut al menú de l'exàmen, a continuació marqui un numero del 1 al 4 per accedir al examen, i un 0 per sortir.");
                numero = Convert.ToInt32(Console.ReadLine());
                switch (numero)
                {
                    case 1:
                        Console.Clear();
                        Exercici1();
                        Console.WriteLine("Introdueixi una tecla per a sortir.");
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.Clear();
                        Exercici2();
                        Console.WriteLine("Introdueixi una tecla per a sortir.");
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Clear();
                        Exercici3();
                        Console.WriteLine("Introdueixi una tecla per a sortir.");
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Clear();
                        Exercici4();
                        Console.WriteLine("Introdueixi una tecla per a sortir.");
                        Console.ReadKey();
                        break;
                }
            } while (numero != 0);
        }

        private static void Exercici1()
        {
            int j = 0;
            Console.WriteLine("Introdueixi la mida del vector:");
            int mida = Convert.ToInt32(Console.ReadLine());
            int digitAcaba;
            int[] primerVector = new int[mida];
            var rnd = new Random();
            Console.WriteLine("Valors Primer Vector:");
            for (int i = 0; i < primerVector.Length; i++)
            {
                primerVector[i] = rnd.Next(1,301);
                Console.Write(" "+primerVector[i]+" ");
            }
            int[] segonVector = new int[mida];
            Console.WriteLine();
            Console.WriteLine("Introdueixi digit que acabi");
            digitAcaba = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Valors Segon vector:");
            foreach (var numero in primerVector)
            {
                if (numero % 10 == digitAcaba)
                {
                    segonVector[j] = numero;
                    Console.Write(" "+segonVector[j]+" ");
                    j++;
                    
                }
            }
            Console.WriteLine("");
        }

        private static void Exercici2()
        {
            int traspas;
            int valorMin = 11;
            int valorMax = -11;
            int[,] vectorA = new int[4, 4];
            int[,] vectorB = new int[4, 4];
            var rnd = new Random();
            for (int i = 0; i < vectorA.GetLength(0); i++)
            {
                for (int j = 0; j < vectorA.GetLength(1); j++)
                {
                    vectorA[i, j] = rnd.Next(-10, 11);
                    vectorB[i, j] = rnd.Next(-10, 11);
                    if (vectorA[i, j] <valorMin)
                    {
                        valorMin = vectorA[i, j];
                    }
                    if (vectorB[i, j] >valorMax)
                    {
                        valorMax = vectorB[i, j];
                    }
                }
            }
            Console.WriteLine("Valors A");
            foreach (var numero in vectorA)
            {
                Console.Write(" "+numero+" ");
            }
            Console.WriteLine("");
            Console.WriteLine("Valors B");
            foreach (var numero in vectorB)
            {
                Console.Write(" "+numero+" ");
            }
            Console.WriteLine("");
            for (int i = 0; i < vectorA.GetLength(0); i++)
            {
                for (int j = 0; j < vectorA.GetLength(1); j++)
                {
                    if (vectorA[i, j] == valorMin)
                    {
                        for (int k = 0; k < vectorA.GetLength(0); k++)
                        {
                            for (int l = 0; l < vectorA.GetLength(1); l++)
                            {
                                if (vectorB[k, l] == valorMax)
                                {
                                    traspas = vectorA[i, j];
                                    vectorA[i,j] = vectorB[k, l];
                                    vectorB[k, l] = traspas;
                                }
                            }
                        }
                    }
                }
            }
            Console.WriteLine("Valors A cambiats");
            foreach (var numero in vectorA)
            {
                Console.Write(" "+numero+" ");
            }
            Console.WriteLine("");
            Console.WriteLine("Valors B cambiats");
            foreach (var numero in vectorB)
            {
                Console.Write(" "+numero+" ");
            }
            Console.WriteLine("");

        }

        private static void Exercici3()
        {
            string paraula;
            do
            {
                Console.WriteLine("Introdueixi 10 lletres juntes");
                paraula = Console.ReadLine();
            } while (paraula != null && paraula.Length != 10);

            if (paraula != null)
            {
                char[] vector = paraula.ToCharArray();
                Array.Sort(vector);
                paraula = "";
                for (int j=0;j<vector.Length;j++)
                {
                    paraula += Convert.ToString(vector[j]);
                }
            }

            Console.WriteLine(paraula);
        }
        private static void Exercici4()
        {
            string fraseGuardada="e";
            string fraseActual;
            int comptadorLletraAnterior=-1,comptadorLletraActual=0;
            Console.WriteLine("Introdueixi una frase:");
            fraseActual = Console.ReadLine();
            do
            {
                if (fraseActual != null)
                    for (int j = 0; j < fraseActual.Length; j++)
                    {
                        if (fraseActual[j] == 'a' || fraseActual[j] == 'A')
                        {
                            comptadorLletraActual += 1;
                        }
                    }

                if (comptadorLletraActual>comptadorLletraAnterior)
                {
                    comptadorLletraAnterior = comptadorLletraActual;
                    fraseGuardada = fraseActual;
                }

                comptadorLletraActual = 0;
                Console.WriteLine($"La frase amb més 'a' és: \"{fraseGuardada}\"");
                Console.WriteLine($"Té {comptadorLletraAnterior} 'a'.");
                Console.WriteLine("Introdueixi una frase:");
                fraseActual = Console.ReadLine();
            } while (fraseActual != "end");
        }
    }
}